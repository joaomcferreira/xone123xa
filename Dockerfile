FROM debian:bullseye-slim

MAINTAINER João Miguel Ferreira <joao.miguel.c.ferreira@gmail.com>

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /app

RUN apt update
RUN apt upgrade -y
RUN apt install -y ruby-full

COPY src /app

CMD ["ruby", "/app/bin/run.rb", "12345"]
